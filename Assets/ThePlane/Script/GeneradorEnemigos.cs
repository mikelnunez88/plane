﻿using UnityEngine;
using System.Collections;

public class GeneradorEnemigos : MonoBehaviour 
{
	public GameObject[] obj;
	public float tiempoMin=1f;
	public float tiempoMax=2f;

	
	public float fVelocidad=20.0f;
	bool bArriba=false;

	// Use this for initialization
	void Start () 
	{
		Generar ();
	}
	
	// Update is called once per frame
	void Update () 
	
	{
		
		if (bArriba == false) 
		{
			transform.Translate (Vector2.up*-1 * fVelocidad * Time.deltaTime);
			
			if (transform.position.y < -4) 
			{
				bArriba = true;	
			}
		} 
		
		else 
		{
			transform.Translate (Vector2.up * fVelocidad * Time.deltaTime);
			
			if (transform.position.y > 4)
			{
				bArriba = false;
			}
			
		}

	
	}

	void Generar()
	{
		Instantiate (obj [Random.Range (0, obj.Length)], transform.position, Quaternion.identity);
		Invoke ("Generar", Random.Range (tiempoMin, tiempoMax));
	}


}
