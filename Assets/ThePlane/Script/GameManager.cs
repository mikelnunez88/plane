﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public ThePlaneScript thePlane;
	private Vector3 planeStartPoint;

	private DistanceManager theDistanceManager;

	// Use this for initialization
	void Start () 
	{
		planeStartPoint = thePlane.transform.position;
		theDistanceManager = FindObjectOfType<DistanceManager> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void RestartGame()
	{
		StartCoroutine ("RestartGameGo");
	}

	public IEnumerator RestartGameGo()
	{
		theDistanceManager.distanceIncreasing = false;
		thePlane.gameObject.SetActive (false);
		yield return new WaitForSeconds (0.5f);

		thePlane.transform.position = planeStartPoint;
		thePlane.gameObject.SetActive (true);

		theDistanceManager.distanceCount = 0;
		theDistanceManager.distanceIncreasing = true;
	}
}
