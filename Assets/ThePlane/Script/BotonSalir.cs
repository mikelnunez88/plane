﻿using UnityEngine;
using System.Collections;

public class BotonSalir : MonoBehaviour 
{

	public GUITexture Boton;

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
		foreach (Touch touch in Input.touches) 
		{
			if(touch.phase == TouchPhase.Began && Boton.HitTest (touch.position))
			{
				Application.Quit ();
			}	
		}
	}
}