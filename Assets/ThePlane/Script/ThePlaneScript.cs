﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class ThePlaneScript : MonoBehaviour
{
	Vector3 velocidad = Vector3.zero;
	public Vector3 gravedad;
	public Vector3 velocidadAleteo;
	bool aleteo= false;
	public float velocidadMaxima;
	public EdificiosScript Tubo1;
	public EdificiosScript Tubo2;
	//private Animator anim;
	private bool juegoTerminado;
	private bool juegoIniciado;
	public RectTransform botonesMenu;
	public AudioClip sonidoMuerte;
	public GameObject deathParticle;
	public GameObject Windy;

	private Rigidbody2D myRigidbody;
	public float moveSpeed;

	public Text Instrucciones;

	public GameManager theGameManager;
	public DistanceManager theDistanceManager;
	public GeneradorEnemigos theGeneradorEnemigos;
	public GameObject GeneradorEnemigosDestroy;


	// Use this for initialization
	void Start () 
	{
		//anim = this.gameObject.GetComponent<Animator> ();
		myRigidbody = GetComponent <Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		int numPresiones = 0;
		foreach(Touch toque in Input.touches)
		{
			if(toque.phase != TouchPhase.Ended && toque.phase != TouchPhase.Canceled)
				numPresiones++;
			
		}
	
		if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) || numPresiones > 0)
		{
			if(juegoTerminado == false)
			{
				myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.x);
				theDistanceManager.gameObject.SetActive(true);
				theGeneradorEnemigos.gameObject.SetActive(true);
				//Windy.gameObject.SetActive (true);


			    aleteo= true;
			    juegoIniciado=true;

				Instrucciones.gameObject.SetActive(false);
				//Instantiate(Windy, this.transform.position, this.transform.rotation);

				Tubo1.juegoIniciado=true;
				Tubo2.juegoIniciado=true;

			}
		}

		if(juegoTerminado == true)
		{
			//Invoke("MostrarMenu",1f);
			theGeneradorEnemigos.gameObject.SetActive(false);
			MostrarMenu();
		}
	}

	void FixedUpdate ()
	{
		if (juegoIniciado) 
		{
			velocidad += gravedad * Time.deltaTime;

			if (aleteo == true) {
				aleteo = false;
				velocidad.y = velocidadAleteo.y;
			}

			transform.position += velocidad * Time.deltaTime;
			float angulo = 0;

			if (velocidad.y >= 0) {
				angulo = Mathf.Lerp (0, 25, velocidad.y / velocidadMaxima);
			} else {
				angulo = Mathf.Lerp (0, -30, -velocidad.y / velocidadMaxima);

			}

			transform.rotation = Quaternion.Euler (0, 0, angulo);
		}
	}

	void OnCollisionEnter2D(Collision2D colision)
	{
		if(colision.gameObject.name == "TuboAbajo" | colision.gameObject.name == "TuboArriba" | colision.gameObject.name == "Suelo")
		{
			Tubo1.velocidad = new Vector3(0,0,0);
			Tubo2.velocidad = new Vector3(0,0,0);

			//anim.SetTrigger("JuegoTerminado");
			Instantiate(deathParticle, this.transform.position, this.transform.rotation);
			theGameManager.RestartGame();
			theDistanceManager.gameObject.SetActive(false);


			juegoTerminado = true;
			ReproducirSonido (sonidoMuerte);
			//Destroy(gameObject);
			Invoke("DestruirPlane",0.01f);


		}

		if(colision.gameObject.name == "PlaneEnemigo(Clone)")
		{
			Instantiate(deathParticle, this.transform.position, this.transform.rotation);
			theGameManager.RestartGame();
			theDistanceManager.gameObject.SetActive(false);
			
			
			juegoTerminado = true;
			ReproducirSonido (sonidoMuerte);
			//Destroy(gameObject);
			Invoke("DestruirPlane",0.01f);
			gravedad = new Vector3(0,0,0);

		}
		if(colision.gameObject.name == "TuboAbajo")
		{
			colision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
			ReproducirSonido (sonidoMuerte);


		}
		if(colision.gameObject.name == "Suelo")
		{
			gravedad = new Vector3(0,0,0);

		}
	}

	public void MostrarMenu()
	{
		Destroy (GeneradorEnemigosDestroy.gameObject);



		botonesMenu.gameObject.SetActive (true);
	}

	private void ReproducirSonido(AudioClip clipOriginal)
	{
		AudioSource.PlayClipAtPoint(clipOriginal, transform.position);
	}
	

	void DestruirPlane()
	{
		Destroy (gameObject);

	}

/*	void OnGUI()
	{
		const int anchoBoton = 84;
		const int altoBoton = 60;

		if(juegoTerminado)
		{
			GUI.Button(new Rect(Screen.width /2 - (anchoBoton /2),(1.5f *Screen.height/3) - (altoBoton /2), anchoBoton,altoBoton),"REINICIAR");
			{
				Application.LoadLevel("Escena1");
			}
		}
	}*/
}
