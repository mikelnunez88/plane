﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DistanceManager : MonoBehaviour 
{
	public Text distanceText;
	public Text hiDistanceText;

	public float distanceCount;
	public float hiDistanceCount;

	public float pointsPerSecond;

	public bool distanceIncreasing;

	public GeneradorEnemigos theGeneradorEnemigos2;
	public GeneradorEnemigos theGeneradorEnemigos1;
	public GeneradorEnemigos theGeneradorEnemigos3;
	public GeneradorEnemigos theGeneradorEnemigos4;


	// Use this for initialization
	void Start ()
	{
		if(PlayerPrefs.HasKey("HighDistance"))
		{
			hiDistanceCount = PlayerPrefs.GetFloat("HighDistance");
		}
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(distanceIncreasing)
		{
		distanceCount += pointsPerSecond * Time.deltaTime;
		}

		if(distanceCount > hiDistanceCount)
		{
			hiDistanceCount = distanceCount;
			PlayerPrefs.SetFloat("HighDistance", hiDistanceCount);
		}

		distanceText.text = "Distance: " + Mathf.Round(distanceCount);
		hiDistanceText.text = "High Distance: " + Mathf.Round(hiDistanceCount);
	
		distanceText.text = ""+ distanceCount;
		hiDistanceText.text = ""+ hiDistanceCount;

		if (distanceCount >= 25) 
		{
			theGeneradorEnemigos2.gameObject.SetActive(true);

		}
		if (distanceCount >= 35) 
		{
			theGeneradorEnemigos1.gameObject.SetActive(true);
		}

		if (distanceCount >= 55) 
		{
			theGeneradorEnemigos4.gameObject.SetActive(true);

		}
		if (distanceCount >= 70) 
		{
			theGeneradorEnemigos3.gameObject.SetActive(true);
		}


	}
}
