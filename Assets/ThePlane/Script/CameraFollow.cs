﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public ThePlaneScript thePlane;

	private Vector3 lastPlanePosition;
	private float distanceToMove;

	// Use this for initialization
	void Start () 
	{
		thePlane = FindObjectOfType<ThePlaneScript> ();
		lastPlanePosition = thePlane.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		distanceToMove = thePlane.transform.position.x - lastPlanePosition.x;

		transform.position = new Vector3 (transform.position.x + distanceToMove, transform.position.y, transform.position.z);
	
		lastPlanePosition = thePlane.transform.position;
	}
}
