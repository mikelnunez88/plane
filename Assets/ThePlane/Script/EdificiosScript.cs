﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EdificiosScript : MonoBehaviour 
{
	public Vector3 velocidad; 

	public Vector3 distanciaEntreColumnas;

	//public Text puntuacion;

	private bool aumentarPuntuacion = true;

	public bool juegoIniciado;

	//public AudioClip sonidoPunto;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (juegoIniciado == true) 
		{
			moverTubo ();
		}
	}

	private void moverTubo()
	{
		this.transform.position = this.transform.position + (velocidad * Time.deltaTime);
	
		if (this.transform.position.x <= -13.5f) 
		{
			Vector3 posicionTemporal = this.transform.position + distanciaEntreColumnas;

			posicionTemporal.y = Random.Range (-3f, 0.6f);

			this.transform.position = posicionTemporal;

			//aumentarPuntuacion = true;
		}

		if (this.transform.position.x <= -8.98f & aumentarPuntuacion == true) {
			//int puntos = int.Parse (puntuacion.text) + 1;
			//puntuacion.text = puntos.ToString ();
			//aumentarPuntuacion = false;
			//ReproducirSonido (sonidoPunto);
		}
	}

		private void ReproducirSonido(AudioClip clipOriginal)
		{
			AudioSource.PlayClipAtPoint(clipOriginal, transform.position);
		}
	}

